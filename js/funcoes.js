// HOME -> produtos
$(document).ready(function(){
	$('.carousel-blog').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1921,
			settings: {
				slidesToShow: 4,
			}
		}, {
			breakpoint: 1661,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 1025,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});
});

// SOBRE -> equipe
$(document).ready(function(){
	$('.carousel-equipe').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left-2.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right-2.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1920,
			settings: {
				slidesToShow: 4,
			}
		}, {
			breakpoint: 1661,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});
});

// SOBRE -> historia
$(document).ready(function(){
	$('.carousel-descricao').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		fade: true,
		asNavFor: '.slider-nav'
	});

	$('.slider-nav').slick({
		slidesToShow: 6,
	  	slidesToScroll: 1,
	  	asNavFor: '.carousel-descricao',
	  	dots: false,
	  	arrows: false,
	  	centerMode: false,
	  	focusOnSelect: true,
	  	responsive: [
	  	{
	  		breakpoint: 1200,
	  		settings: {
	  			slidesToShow: 4,
	  		}
	  	}, {
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 2,
			}
		}
	  	]
	});
});

//Iniciar animações
AOS.init({
	disable: 'mobile',
	delay: 0,
	duration: 1200,
	once: true,
});